Imports System.Configuration.ConfigurationSettings
Imports System.xml
Imports System.Text.RegularExpressions
Imports System.io
Imports System.Data.OleDb
Imports ntb_FuncLib

Public Class PictRouteJob
    Inherits System.ComponentModel.Component

#Region " Component Designer generated code "

    Public Sub New(ByVal Container As System.ComponentModel.IContainer)
        MyClass.New()

        'Required for Windows.Forms Class Composition Designer support
        Container.Add(Me)
    End Sub

    Public Sub New()
        MyBase.New()

        'This call is required by the Component Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Component overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Component Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Component Designer
    'It can be modified using the Component Designer.
    'Do not modify it using the code editor.
    Friend WithEvents PollTimer As System.Timers.Timer
    Friend WithEvents NewFilesWatcher As System.IO.FileSystemWatcher
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.PollTimer = New System.Timers.Timer
        Me.NewFilesWatcher = New System.IO.FileSystemWatcher
        CType(Me.PollTimer, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NewFilesWatcher, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'PollTimer
        '
        Me.PollTimer.Interval = 1000
        '
        'NewFilesWatcher
        '
        Me.NewFilesWatcher.NotifyFilter = CType((System.IO.NotifyFilters.FileName Or System.IO.NotifyFilters.LastWrite), System.IO.NotifyFilters)
        CType(Me.PollTimer, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NewFilesWatcher, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub

#End Region

    'Control variables
    Dim _logMtx As Threading.Mutex = New Threading.Mutex(False, "PICTURE_LOG")
    Dim _isReady As Threading.AutoResetEvent = New Threading.AutoResetEvent(True)

    'Database stuff
    Dim dsCustomer As DataSet = New DataSet

    'Xml params
    Dim jobId As Integer
    Dim xmlroute_dbFields As XmlNodeList

    'Folders
    Dim databaseOfflinePath As String = AppSettings("DBOfflinePath")
    Dim logPath As String = AppSettings("LogPath")

    Dim inputPath As String
    Dim fileFilter As String
    Dim filterRx As Regex

    Dim subFolder As String
    Dim donePath As String
    Dim dateSub As Boolean = False

    'Event stuff
    Dim pollInterval As Integer = AppSettings("PollingInterval") * 1000

    'Load everything
    Sub Init(ByVal node As XmlNode)

        'Load stuff from xml node
        Try
            jobId = node.Attributes("id").Value
            xmlroute_dbFields = node.SelectNodes("DatabaseField")

            inputPath = node.SelectSingleNode("InputPath").InnerText()

            fileFilter = node.SelectSingleNode("InputPath/@fileFilter").InnerText()
            'Build regex
            filterRx = New Regex(fileFilter.Replace(".", "\.").Replace("*", ".").Replace("?", ".{1}"),RegexOptions.Compiled or RegexOptions.IgnoreCase )

            subFolder = node.SelectSingleNode("SubFolder").InnerText()
            donePath = node.SelectSingleNode("DonePath").InnerText()
            dateSub = (node.SelectSingleNode("DonePath").Attributes("datesub").Value = "true")
        Catch ex As Exception
            LogFile.WriteErr(logPath, "Feil i oppsettsfil:", ex)
        End Try

        'Connection
        Dim cn As OleDbConnection = New OleDbConnection(AppSettings("ConnectionString"))
        
        'SQL command
        Dim command As OleDbCommand = New OleDbCommand
        command.Connection = cn
        command.CommandText = "SELECT * FROM GetCustomerPictures WHERE "

        'Set criteria based on config file
        Dim n As XmlNode
        For Each n In xmlroute_dbFields
            command.CommandText &= n.Attributes("name").Value & " = " & n.Attributes("value").Value & " OR "
        Next

        command.CommandText = command.CommandText.Substring(0, command.CommandText.LastIndexOf(" OR "))

        'Data adapter to fill dataset
        Dim dataAdapter As OleDbDataAdapter = New OleDbDataAdapter
        dataAdapter.SelectCommand = command

        'Fill the set and store offline XML recordset
        Dim strFile As String = databaseOfflinePath & "\dsCustomer-" & jobId & ".xml"
        Try
            dataAdapter.Fill(dsCustomer)
            dsCustomer.WriteXml(strFile, XmlWriteMode.WriteSchema)
        Catch ex As Exception
            dsCustomer.ReadXml(strFile, XmlReadMode.ReadSchema)
        End Try
        cn.Close()

        'Make folders
        Directory.CreateDirectory(inputPath)
        Directory.CreateDirectory(donePath)

        Dim r As DataRow
        For Each r In dsCustomer.Tables(0).Rows
            Dim output As String = ""
            output = r("DefOutPath") & "\" & subFolder
            Directory.CreateDirectory(output)
        Next

        'Set up watcher
        NewFilesWatcher.InternalBufferSize = 4096 * 4
        NewFilesWatcher.Filter = fileFilter
        NewFilesWatcher.Path = inputPath
    End Sub

    'Start timers and events
    Sub Start()
        PollTimer.Interval = 3000
        PollTimer.Enabled = True
        NewFilesWatcher.EnableRaisingEvents = True
    End Sub

    'Stopp stuff
    Sub Quit()
        PollTimer.Enabled = False
        NewFilesWatcher.EnableRaisingEvents = False
        _isReady.WaitOne(10000, False)
    End Sub

    'New file
    Private Sub NewFilesWatcher_Created(ByVal sender As Object, ByVal e As System.IO.FileSystemEventArgs) Handles NewFilesWatcher.Created
        _isReady.WaitOne()

        'Do some files
        DoAllFiles()

        _isReady.Set()
    End Sub

    'Timed check
    Private Sub PollTimer_Elapsed(ByVal sender As Object, ByVal e As System.Timers.ElapsedEventArgs) Handles PollTimer.Elapsed
        _isReady.WaitOne()

        'Do some files
        DoAllFiles()

        PollTimer.Interval = pollInterval
        _isReady.Set()
    End Sub

    'Loop through waiting files
    Sub DoAllFiles()

        'Get all files
        Dim files As String() = Directory.GetFiles(inputPath)

        'Sleep and wait for writes
        If files.GetLength(0) > 0 Then Threading.Thread.Sleep(5000)

        'Loop the files
        Dim f As String
        For Each f In files
            If filterRx.IsMatch(f) Then DoOneFile(f)
        Next

    End Sub

    'Route a picture
    Sub DoOneFile(ByVal filename As String)

        _logMtx.WaitOne()
        Try
            LogFile.WriteLogNoDate(logPath, "-----------------------------------------------------------------------------------------------")
            LogFile.WriteLog(logPath, "Nytt bilde: " & filename)

            'Copy the file
            Dim r As DataRow
            For Each r In dsCustomer.Tables(0).Rows

                Dim output As String = ""
                output = r("DefOutPath") & "\" & subFolder

                Try
                    File.Copy(filename, output & "\" & Path.GetFileName(filename), True)
                    LogFile.WriteLog(logPath, "Kopiert til : " & output & "\" & Path.GetFileName(filename))
                Catch ex As Exception
                    LogFile.WriteLog(logPath, "FEILET til : " & output & "\" & Path.GetFileName(filename))
                    LogFile.WriteErr(logPath, "Kopiering feilet.", ex)
                End Try
            Next

            'Donepath
            Dim done As String = donePath & "\" & Path.GetFileName(filename)
            If dateSub Then
                done = FuncLib.MakeSubDirDate(done, File.GetLastWriteTime(filename))
            End If

            'Clean up
            Try
                File.Copy(filename, done, True)
                File.Delete(filename)
                LogFile.WriteLog(logPath, "Slettet: " & filename)
            Catch ex As Exception
                LogFile.WriteErr(logPath, "Sletting feilet.", ex)
            End Try

        Catch ex As Exception
            LogFile.WriteErr(logPath, "Kopiering feilet.", ex)
        End Try

        _logMtx.ReleaseMutex()

    End Sub
End Class

