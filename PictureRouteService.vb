Imports System.ServiceProcess
Imports System.Configuration.ConfigurationSettings
Imports System.io
Imports System.xml
Imports ntb_FuncLib

Public Class PictureRouteService
    Inherits System.ServiceProcess.ServiceBase

#Region " Component Designer generated code "

    Public Sub New()
        MyBase.New()

        ' This call is required by the Component Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call

    End Sub

    'UserService overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    ' The main entry point for the process
    <MTAThread()> _
    Shared Sub Main()
        Dim ServicesToRun() As System.ServiceProcess.ServiceBase

        ' More than one NT Service may run within the same process. To add
        ' another service to this process, change the following line to
        ' create a second service object. For example,
        '
        '   ServicesToRun = New System.ServiceProcess.ServiceBase () {New Service1, New MySecondUserService}
        '
        ServicesToRun = New System.ServiceProcess.ServiceBase() {New PictureRouteService}

        System.ServiceProcess.ServiceBase.Run(ServicesToRun)
    End Sub

    'Required by the Component Designer
    Private components As System.ComponentModel.IContainer

    ' NOTE: The following procedure is required by the Component Designer
    ' It can be modified using the Component Designer.  
    ' Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        '
        'PictureRouteService
        '
        Me.CanShutdown = True
        Me.ServiceName = "PictureRouteService"

    End Sub

#End Region

    'Variables
    Dim _logMtx As Threading.Mutex = New Threading.Mutex(False, "PICTURE_LOG")

    Dim databaseOfflinePath As String = AppSettings("DBOfflinePath")
    Dim logPath As String = AppSettings("LogPath")

    Dim htRoutes As Hashtable = New Hashtable

    Protected Overrides Sub OnStart(ByVal args() As String)
        ' Add code here to start your service. This method should set things
        ' in motion so your service can do its work.

        'Make folders
        Directory.CreateDirectory(logPath)
        Directory.CreateDirectory(databaseOfflinePath)

        _logMtx.WaitOne()

        'Load config
        Dim config As XmlDocument = New XmlDocument
        Try
            config.Load(AppSettings("ConfigFile"))
        Catch ex As Exception
            LogFile.WriteErr(logPath, "Feil i konfigurasjonsfil: ", ex)
        End Try

        'Loop and initiate jobs
        Dim node As XmlNode
        Dim list As XmlNodeList
        list = config.SelectNodes("/pictureJobs/job[@disabled='false']")

        LogFile.WriteLogNoDate(logPath, "-----------------------------------------------------------------------------------------------")
        LogFile.WriteLog(logPath, "PictureRoute starter...")

        'Set up each job
        For Each node In list
            Dim objRoute As PictRouteJob = New PictRouteJob

            Try
                objRoute.Init(node)
                objRoute.Start()
                htRoutes.Add(node.Attributes("id").Value, objRoute)

                LogFile.WriteLog(logPath, "Bilde-Ruteobjekt initiert for mappen '" & node.FirstChild.InnerText & "'.")
            Catch ex As Exception
                LogFile.WriteErr(logPath, "Feil i initiering: ", ex)
            End Try

        Next

        LogFile.WriteLog(logPath, "PictureRoute initiert og startet.")
        _logMtx.ReleaseMutex()

    End Sub

    Protected Overrides Sub OnStop()
        ' Add code here to perform any tear-down necessary to stop your service.
        Dim objRoute As PictRouteJob

        For Each objRoute In htRoutes.Values
            objRoute.Quit()
            objRoute = Nothing
        Next

        htRoutes = Nothing

        LogFile.WriteLogNoDate(logPath, "-----------------------------------------------------------------------------------------------")
        LogFile.WriteLog(logPath, "PictureRoute er stopped.")
    End Sub

    Protected Overrides Sub OnShutdown()
        OnStop()
    End Sub
End Class
